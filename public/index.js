const express = require("express");
const app = express();
app.set('view engine', 'ejs'); // Set EJS as the template engine
app.use(express.static('public')); // Serve static files from the "public" directory
app.use(express.json());

app.get('/', (req, res) => {
    res.render("view");
})
app.listen(3000, ()=>{
    console.log("Server started at ", 3000);
})